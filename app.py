# -*- coding: utf-8 -*-
__author__ = 'David Hsu'

from Tkinter import *
import tkMessageBox
from convert import Convert


class Application(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master, height=500)
        self.quit_button = Button(self)
        self.load_database_button = Button(self)
        self.edit_model_button = Button(self)
        self.save_model_button = Button(self)
        self.export_database_button = Button(self)
        self.pack()
        self.create_widgets()

    @staticmethod
    def say_hi():
        print "hi there, everyone!"

    @staticmethod
    def load_database():
        # 加载数据库
        tkMessageBox.showinfo("Say Hello", "Hello World")

    @staticmethod
    def edit_model():
        # 调用Excel编辑模型
        convert = Convert()
        convert.load_from_sqlite()

    def create_widgets(self):
        # 创建界面组件
        self.load_database_button["text"] = "加载数据库",
        self.load_database_button["command"] = self.load_database
        self.load_database_button.pack({"side": "left"})

        self.edit_model_button["text"] = "编辑模型",
        self.edit_model_button["command"] = self.edit_model
        self.edit_model_button.pack({"side": "left"})

        self.save_model_button["text"] = "保存模型",
        self.save_model_button["command"] = self.say_hi
        self.save_model_button.pack({"side": "left"})

        self.export_database_button["text"] = "导出数据库",
        self.export_database_button["command"] = self.say_hi
        self.export_database_button.pack({"side": "left"})

        self.quit_button["text"] = "退出"
        self.quit_button["fg"] = "red"
        self.quit_button["command"] = self.quit
        self.quit_button.pack({"side": "left"})


root = Tk()
app = Application(master=root)
app.mainloop()
root.destroy()
