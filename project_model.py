# -*- coding: utf-8 -*-
__author__ = 'David Hsu'


class PerformanceHistory:
    def __init__(self):
        self.paramID = ""
        self.deviceID = ""
        self.protocolID = ""


class ModbusPoint(PerformanceHistory):
    def __init__(self):
        PerformanceHistory.__init__(self)
        self.ip = "192.168.1.1"
        self.code = 3
        self.outParam = 1
        self.headAddress = 16650


class ProjectModel:
    def __init__(self):
        self.devices = []
        self.points = []
        self.device_types = []
        self.point_types = []

    def add_device(self, point):
        self.points.append(point)
