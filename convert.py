# -*- coding: utf-8 -*-
__author__ = 'David Hsu'

from project_model import ProjectModel
from openpyxl import Workbook
from openpyxl import load_workbook
import sqlite3


class Convert:

    def __init__(self):
        self.projectModel = ProjectModel()

    def load_from_sqlite(self):
        self.projectModel = ProjectModel()
        # 打开数据库，读取数据并保存到模型中
        db_connect = sqlite3.connect('weldtech.db')
        db_connect.row_factory = sqlite3.Row
        db_cursor = db_connect.cursor()
        db_cursor.execute('SELECT * FROM device_status')

        for row in db_cursor:
            # 提取点位
            print(row)
            print(row.keys())

        db_cursor.close()

    def save_to_sqlite(self):
        # 将模型保存到数据库中
        db_connect = sqlite3.connect('weldtech.db')
        db_cursor = db_connect.cursor()
        db_cursor.close()
        self.projectModel = ProjectModel()

    def load_from_excel(self):
        self.projectModel = ProjectModel()
        book = load_workbook('project_model.xlsx')
        print book.get_sheet_names()

    def save_to_excel(self):
        book = Workbook(write_only=True)
        book.save('project_model.xlsx')
        self.projectModel = ProjectModel()

    def convert_sqlite_to_excel(self):
        self.load_from_sqlite()
        self.save_to_excel()

    def convert_excel_to_sqlite(self):
        self.load_from_excel()
        self.save_to_sqlite()


